db.new_fruit_collection.insertMany([
	{
		name: 'Banana',
		supplier: 'Farmer Fruits Inc.',
		stocks: 30,
		price: 20,
		onSale: true
	},
	{
		name: 'Mango',
		supplier: 'Mango Magic Inc.',
		stocks: 50,
		price: 70,
		onSale: true
	},
	{
		name: 'Dragon Fruit',
		supplier: 'Farmer Fruits Inc.',
		stocks: 10,
		price: 60,
		onSale: true
	},
	{
		name: 'Grapes',
		supplier: 'Fruity Co.',
		stocks: 30,
		price: 100,
		onSale: true
	},
	{
		name: 'Apple',
		supplier: 'Apple Valley',
		stocks: 0,
		price: 20,
		onSale: false
	},
	{
		name: 'Papaya',
		supplier: 'Fruity Co.',
		stocks: 15,
		price: 60,
		onSale: true
	}
])

// 2
db.new_fruit_collection.find({"onSale":true}).count()

//3
db.new_fruit_collection.find({"stocks":{$gt:20}}).count()

//4
db.new_fruit_collection.aggregate(
    [
	{
		$match: {
			"onSale": true
		}
	},
	{
		$group: {
			"_id": "$supplier",
			"avg_price": {
				$avg: "$price"
			}
		}
	}
])

//5
db.new_fruit_collection.aggregate(
    [
	{
		$group: {
			"_id": "$supplier",
			"max_price": {
				$max: "$price"
			}
		}
	}
])


//6
db.new_fruit_collection.aggregate(
    [
	{
		$group: {
			"_id": "$supplier",
			"min_price": {
				$min: "$price"
			}
		}
	}
])

//7